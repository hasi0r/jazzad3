<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="domain.BasicParameters" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator wypłat</title>
</head>
<body>
	<jsp:useBean id="basicParameters" class="domain.BasicParameters" scope="session" />	
	<jsp:setProperty property="*" name="basicParameters" />
	
	<%
		if (basicParameters.getWorkType().equals("uop")){
			response.sendRedirect("uop.jsp");
		}
		if (basicParameters.getWorkType().equals("zlecenie")){
	%>
		<form action="contract.jsp">
			<table>
				<tr><td><label>Składka zdrowotna: </label></td>
					<td><input type="radio" name="zdrowotna" value="true"/> Tak </td>
					<td><input type="radio" name="zdrowotna" value="false"/> Nie </td>
				</tr>
				<tr><td><label>Składka chorobowa: </label></td>
					<td><input type="radio" name="chorobowa" value="true"/> Tak </td>
					<td><input type="radio" name="chorobowa" value="false"/> Nie </td>
				</tr>
				<tr><td><label>Składka rentowa: </label></td>
					<td><input type="radio" name="rentowa" value="true"/> Tak </td>
					<td><input type="radio" name="rentowa" value="false"/> Nie </td>
				</tr>
				<tr><td><label>Składka emerytalna: </label></td>
					<td><input type="radio" name="emerytalna" value="true"/> Tak </td>
					<td><input type="radio" name="emerytalna" value="false"/> Nie </td>
				</tr>
				<tr><td><label>Koszty uzyskania przychodu: </label></td>
					<td><input type="radio" name="costs" value="20" /> 20% </td>
					<td><input type="radio" name="costs" value="50" /> 50% </td>
				</tr> 
				<tr><td><input type="submit" value="Nastepny krok"></td></tr>
			</table>
		</form> 
	<%
		}
	%>
	<%
		if (basicParameters.getWorkType().equals("dzielo")){
	%>
		<form action="volume.jsp">
			<table>
				<tr><td><label>Koszty uzyskania przychodu: </label></td>
					<td><input type="radio" name="costs" value="20" /> 20% </td>
					<td><input type="radio" name="costs" value="50" /> 50% </td>
				</tr> 
				<tr><td><input type="submit" value="Nastepny krok"></td></tr>
			</table>
		</form> 
	<%
		}
	%>
</body>
</html>