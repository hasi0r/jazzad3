<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator wypłat</title>
</head>
<body>
	<jsp:useBean id="basicParameters" class="domain.BasicParameters" scope="session" />
	<jsp:useBean id="contractSalary" class="domain.ContractSalary" scope="session" />
	
	<jsp:setProperty property="*" name="contractSalary"/>
	
	<%
		contractSalary.setBasicParameters(basicParameters);
		contractSalary.countSalary();
	%>
	<h1>Umowa zlecenie</h1>
	<style type="text/css">
	.tg  {border-collapse:collapse;border-spacing:0;}
	.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
	.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
	.tg .tg-baqh{text-align:center;vertical-align:top}
	.tg .tg-amwm{font-weight:bold;text-align:center;vertical-align:top}
	</style>
	<table class="tg">
  	<tr>
    	<th class="tg-amwm" rowspan="2">Brutto</th>
    	<th class="tg-baqh" colspan="4">Ubezpieczenie</th>
    	<th class="tg-baqh" rowspan="2">Koszt uzyskania przychodu</th>
    	<th class="tg-baqh" rowspan="2">Podstawa opodatkowania</th>
    	<th class="tg-baqh" rowspan="2">Zaliczka na PIT</th>
    	<th class="tg-amwm" rowspan="2">Netto</th>
  	</tr>
  	<tr>
    	<td class="tg-baqh">emerytalne</td>
    	<td class="tg-baqh">rentowe</td>
    	<td class="tg-baqh">chorobowe</td>
    	<td class="tg-baqh">zdrowotne</td>
  	</tr>
  	<tr>
    	<td class="tg-amwm"><%=String.format("%.2f",contractSalary.getCountedBrutto()) %></td>
    	<td class="tg-baqh"><%=String.format("%.2f",contractSalary.getCountedPension()) %></td>
    	<td class="tg-baqh"><%=String.format("%.2f",contractSalary.getCountedSocial()) %></td>
    	<td class="tg-baqh"><%=String.format("%.2f",contractSalary.getCountedIll()) %></td>
    	<td class="tg-baqh"><%=String.format("%.2f",contractSalary.getCountedHealth()) %></td>
    	<td class="tg-baqh"><%=String.format("%.2f",contractSalary.getCountedCosts()) %></td>
    	<td class="tg-baqh"><%=String.format("%.2f",contractSalary.getCountedPITBase()) %></td>
    	<td class="tg-baqh"><%=String.format("%.2f",contractSalary.getCountedPIT()) %></td>
    	<td class="tg-amwm"><%=String.format("%.2f",contractSalary.getCountedNetto()) %></td>
  	</tr>
	</table>
<br>
<form action="/">
    <input type="submit" value="Wróć do strony głównej">
</form>

</body>
</html>