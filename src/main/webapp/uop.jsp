<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.lang.Math" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator wypłat</title>
</head>
<body>
	<jsp:useBean id="basicParameters" class="domain.BasicParameters" scope="session"/>
	<jsp:useBean id="uopSalary" class="domain.UOPSalary" scope="session" />
	
	<%
	uopSalary.setBasicParameters(basicParameters);
	uopSalary.countSalary();
	%>
	
	<style type="text/css">
	.tg  {border-collapse:collapse;border-spacing:0;}
	.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
	.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
	.tg .tg-s6z2{text-align:center}
	.tg .tg-baqh{text-align:center;vertical-align:top}
	.tg .tg-hgcj{font-weight:bold;text-align:center}
	.tg .tg-amwm{font-weight:bold;text-align:center;vertical-align:top}
	</style>
	<table class="tg">
  	<tr>
    	<th class="tg-hgcj" rowspan="2"></th>
    	<th class="tg-hgcj" rowspan="2">Brutto</th>
    	<th class="tg-s6z2" colspan="4">Ubezpieczenie</th>
    	<th class="tg-s6z2" rowspan="2">Podstawa opodatkowania</th>
    	<th class="tg-s6z2" rowspan="2">Zaliczka na PIT</th>
    	<th class="tg-hgcj" rowspan="2">Netto</th>
  	</tr>
  	<tr>
    	<td class="tg-s6z2">emerytalne</td>
    	<td class="tg-s6z2">rentowe</td>
    	<td class="tg-s6z2">chorobowe</td>
    	<td class="tg-s6z2">zdrowotne</td>
  	</tr>
  	<%
  	String[] months = {"Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec",
  			"Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień"};
  		for (int i=0; i<12 ; i++){
  	%>	
  	<tr>
    	<td class="tg-hgcj"><%=months[i] %></td>
    	<td class="tg-hgcj"><%=String.format("%.2f", uopSalary.getCountedBrutto()) %></td>
    	<td class="tg-s6z2"><%=String.format("%.2f", uopSalary.getCountedPension()) %></td>
    	<td class="tg-s6z2"><%=String.format("%.2f", uopSalary.getCountedSocial()) %></td>
    	<td class="tg-s6z2"><%=String.format("%.2f", uopSalary.getCountedIll()) %></td>
    	<td class="tg-s6z2"><%=String.format("%.2f", uopSalary.getCountedHealth()) %></td>
    	<td class="tg-s6z2"><%=String.format("%.2f", uopSalary.getCountedPITBase()) %></td>
    	<td class="tg-s6z2"><%=String.format("%.2f", uopSalary.getCountedPIT()) %></td>
    	<td class="tg-hgcj"><%=String.format("%.2f", uopSalary.getCountedNetto()) %></td>
  	</tr>
  	<%		
  		}
  	%>
	</table>
<br>
<form action="/">
    <input type="submit" value="Wróć do strony głównej">
</form>

</body>
</html>