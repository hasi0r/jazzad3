<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator wypłat</title>
</head>
<body>
	<jsp:useBean id="basicParameters" class="domain.BasicParameters" scope="session" />
	<jsp:useBean id="volumeSalary" class="domain.VolumeSalary" scope="session" />
	
	<jsp:setProperty property="*" name="volumeSalary"/>
	
	<%
	volumeSalary.setBasicParameters(basicParameters);
	volumeSalary.countSalary();
	%>
		<h1>Umowa o dzieło</h1><br>
	<table border="1">
		<tr><td align="center"><strong>Brutto</strong></td>
			<td align="center">Koszt uzyskania przychodu</td>
			<td align="center">Podstawa opodatkowania</td>
			<td align="center">Zaliczka na PIT</td>
			<td align="center"><strong>Netto</strong></td></tr>
		<tr><td align="center"><strong><%=String.format("%.2f", volumeSalary.getCountedBrutto()) %></strong></td>
			<td align="center"><%=String.format("%.2f",volumeSalary.getCountedCosts()) %></td>
			<td align="center"><%=String.format("%.2f",volumeSalary.getCountedPITBase()) %></td>
			<td align="center"><%=String.format("%.2f",volumeSalary.getCountedPIT()) %></td>
			<td align="center"><%=String.format("%.2f",volumeSalary.getCountedNetto()) %></td></tr>
	</table>
<br>
<form action="/">
    <input type="submit" value="Wróć do strony głównej">
</form>	

</body>
</html>