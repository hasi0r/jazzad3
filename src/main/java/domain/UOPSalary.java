package domain;

public class UOPSalary extends Salary implements ISalaryCount {
	private BasicParameters basicParameters;
	private double countedPension;
	private double countedSocial;
	private double countedIll;
	private double countedHealth;
	
	
	public double getCountedPension() {
		return countedPension;
	}

	public void setCountedPension(double countedPension) {
		this.countedPension = countedPension;
	}

	public double getCountedSocial() {
		return countedSocial;
	}

	public void setCountedSocial(double countedSocial) {
		this.countedSocial = countedSocial;
	}

	public double getCountedIll() {
		return countedIll;
	}

	public void setCountedIll(double countedIll) {
		this.countedIll = countedIll;
	}

	public double getCountedHealth() {
		return countedHealth;
	}

	public void setCountedHealth(double countedHealth) {
		this.countedHealth = countedHealth;
	}

	public BasicParameters getBasicParameters() {
		return basicParameters;
	}

	public void setBasicParameters(BasicParameters basicParameters) {
		this.basicParameters = basicParameters;
	}

	@Override
	public void countSalary() {
		if (this.basicParameters.getSalaryType().equals("brutto")){	
			countedBrutto = this
					.basicParameters
					.getSalaryValue();
			
			countedPension = countedBrutto * .0976;
			countedSocial = countedBrutto * .015;
			countedIll = countedBrutto * .0245;
			
			double countedSoc = countedPension + countedSocial + countedIll;
			
			countedHealth = (countedBrutto - countedSoc) * .09;
			countedPITBase = Math.round(countedBrutto - countedSoc - 111.25);
			countedPIT = Math.round((countedPITBase * .18) - 46.33 - ((countedBrutto-countedSoc)*.0775));
			countedNetto = countedBrutto - countedSoc - countedHealth - countedPIT;
		} else{
			countedNetto = this
					.basicParameters
					.getSalaryValue();
			
			countedBrutto = Math.round((countedNetto-66.355)/0.69679115);
			countedPension = countedBrutto * .0976;
			countedSocial = countedBrutto * .015;
			countedIll = countedBrutto * .0245;
			
			double countedSoc = countedPension + countedSocial + countedIll;
			
			countedHealth = (countedBrutto - countedSoc) * .09;
			countedPITBase = Math.round(countedBrutto - countedSoc - 111.25);
			countedPIT = Math.round((countedPITBase * .18) - 46.33 - ((countedBrutto-countedSoc)*.0775));
		}
		
	}	
}
