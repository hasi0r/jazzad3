package domain;

public class VolumeSalary extends Salary implements ISalaryCount {
	private BasicParameters basicParameters;

	
	public BasicParameters getBasicParameters() {
		return basicParameters;
	}

	public void setBasicParameters(BasicParameters basicParameters) {
		this.basicParameters = basicParameters;
	}

	@Override
	public void countSalary() {
		if (this.basicParameters.getSalaryType().equals("brutto")){
			countedBrutto = this
					.getBasicParameters()
					.getSalaryValue();
			
			countedCosts = countedBrutto * this.getCosts() / 100;
			countedPITBase = countedBrutto - countedCosts;
			countedPIT = countedPITBase * .18;
			countedNetto = countedBrutto - countedPIT;
		} else{ 
			countedNetto = this
					.basicParameters
					.getSalaryValue();
			
			if (this.getCosts()==50){
				countedBrutto = countedNetto / .91;
			}
			else{
				countedBrutto = countedNetto / .856;
			}
			
			countedCosts = countedBrutto * this.getCosts() /100;
			countedPITBase = countedBrutto - countedCosts;
			countedPIT = countedPITBase * .18;
		}

	}

	public double getCountedBrutto() {
		return countedBrutto;
	}

	public void setCountedBrutto(double countedBrutto) {
		this.countedBrutto = countedBrutto;
	}

	public double getCountedCosts() {
		return countedCosts;
	}

	public void setCountedCosts(double countedCosts) {
		this.countedCosts = countedCosts;
	}

	public double getCountedNetto() {
		return countedNetto;
	}

	public void setCountedNetto(double countedNetto) {
		this.countedNetto = countedNetto;
	}

	public double getCountedPIT() {
		return countedPIT;
	}

	public void setCountedPIT(double countedPIT) {
		this.countedPIT = countedPIT;
	}

	public double getCountedPITBase() {
		return countedPITBase;
	}

	public void setCountedPITBase(double countedPITBase) {
		this.countedPITBase = countedPITBase;
	}

}
