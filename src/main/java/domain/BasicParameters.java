package domain;

public class BasicParameters {
	private String workType;
	private int year;
	private String salaryType;
	private double salaryValue;
	
	public String getWorkType() {
		return workType;
	}
	public void setWorkType(String workType) {
		this.workType = workType;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getSalaryType() {
		return salaryType;
	}
	public void setSalaryType(String salaryType) {
		this.salaryType = salaryType;
	}
	public double getSalaryValue() {
		return salaryValue;
	}
	public void setSalaryValue(double salaryValue) {
		this.salaryValue = salaryValue;
	}
	

	
	
}
