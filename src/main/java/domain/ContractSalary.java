package domain;

public class ContractSalary extends Salary implements ISalaryCount{
	private BasicParameters basicParameters;
	private boolean emerytalna;
	private boolean rentowa;
	private boolean chorobowa;
	private boolean zdrowotna;
	private double countedPension;
	private double countedSocial;
	private double countedIll;
	private double countedHealth;
	
	public double getCountedPension() {
		return countedPension;
	}

	public void setCountedPension(double countedPension) {
		this.countedPension = countedPension;
	}

	public double getCountedSocial() {
		return countedSocial;
	}

	public void setCountedSocial(double countedSocial) {
		this.countedSocial = countedSocial;
	}

	public double getCountedIll() {
		return countedIll;
	}

	public void setCountedIll(double countedIll) {
		this.countedIll = countedIll;
	}

	public double getCountedHealth() {
		return countedHealth;
	}

	public void setCountedHealth(double countedHealth) {
		this.countedHealth = countedHealth;
	}

	public boolean getEmerytalna() {
		return emerytalna;
	}

	public void setEmerytalna(boolean emerytalna) {
		this.emerytalna = emerytalna;
	}

	public boolean getRentowa() {
		return rentowa;
	}

	public void setRentowa(boolean rentowa) {
		this.rentowa = rentowa;
	}

	public boolean getChorobowa() {
		return chorobowa;
	}

	public void setChorobowa(boolean chorobowa) {
		this.chorobowa = chorobowa;
	}

	public boolean getZdrowotna() {
		return zdrowotna;
	}

	public void setZdrowotna(boolean zdrowotna) {
		this.zdrowotna = zdrowotna;
	}

	@Override
	public void countSalary() {
		if (this.basicParameters.getSalaryType().equals("brutto")){	
			countedBrutto = this
					.basicParameters
					.getSalaryValue();
			if (this.getEmerytalna()){
				countedPension = countedBrutto * .0976;
			}
			if (this.getRentowa()){
				countedSocial = countedBrutto * .015;
			}
			if (this.getChorobowa()){
				countedIll = countedBrutto * .0245;
			}
			
			countedCosts = (countedBrutto - countedPension - countedSocial - countedIll) 
					* this.getCosts() / 100;
			countedPITBase = Math.round(countedBrutto - countedPension - countedSocial - countedIll - countedCosts);
			
			if (this.getZdrowotna()){
				countedHealth = (countedBrutto - countedPension - countedSocial - countedIll) * .09;
				countedPIT = Math.round((countedPITBase * .18) - 
						((countedBrutto - countedPension - countedSocial - countedIll)*.0775));
			}
			else{
				countedPIT = Math.round(countedPITBase * .18);
			}
			countedNetto = countedBrutto - countedPension - countedSocial - countedIll - countedHealth - countedPIT;

		} else{
				countedNetto = this
						.basicParameters
						.getSalaryValue();
				countedBrutto = Math.round((countedNetto-66.355)/0.69679115);
				
				
				if (this.getEmerytalna()){
					countedPension = countedBrutto * .0976;
				}
				if (this.getRentowa()){
					countedSocial = countedBrutto * .015;
				}
				if (this.getChorobowa()){
					countedIll = countedBrutto * .0245;
				}
				countedCosts = (countedBrutto - countedPension - countedSocial - countedIll) 
						* this.getCosts() / 100;
				countedPITBase = Math.round(countedBrutto - countedPension - countedSocial - countedIll - countedCosts);
				if (this.getZdrowotna()){
					countedHealth = (countedBrutto - countedPension - countedSocial - countedIll) * .09;
					countedPIT = Math.round((countedPITBase * .18) - 
							((countedBrutto - countedPension - countedSocial - countedIll)*.0775));
				}
				else{
					countedPIT = Math.round(countedPITBase * .18);
				}
				countedNetto = Math.round(countedBrutto - countedPension - countedSocial - countedIll - countedHealth - countedPIT);
		}
		
	}

	public BasicParameters getBasicParameters() {
		return basicParameters;
	}

	public void setBasicParameters(BasicParameters basicParameters) {
		this.basicParameters = basicParameters;
	}

}
