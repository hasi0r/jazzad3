package domain;

public class Salary {
	private double netto;
	private double brutto;
	private double pitBase;
	private double pit;
	private double costs;
	protected double countedBrutto;
	protected double countedCosts;
	protected double countedNetto;
	protected double countedPIT;
	protected double countedPITBase;
	
	
	
	public double getCountedBrutto() {
		return countedBrutto;
	}
	public void setCountedBrutto(double countedBrutto) {
		this.countedBrutto = countedBrutto;
	}
	public double getCountedCosts() {
		return countedCosts;
	}
	public void setCountedCosts(double countedCosts) {
		this.countedCosts = countedCosts;
	}
	public double getCountedNetto() {
		return countedNetto;
	}
	public void setCountedNetto(double countedNetto) {
		this.countedNetto = countedNetto;
	}
	public double getCountedPIT() {
		return countedPIT;
	}
	public void setCountedPIT(double countedPIT) {
		this.countedPIT = countedPIT;
	}
	public double getCountedPITBase() {
		return countedPITBase;
	}
	public void setCountedPITBase(double countedPITBase) {
		this.countedPITBase = countedPITBase;
	}
	public double getNetto() {
		return netto;
	}
	public void setNetto(double netto) {
		this.netto = netto;
	}
	public double getBrutto() {
		return brutto;
	}
	public void setBrutto(double brutto) {
		this.brutto = brutto;
	}
	public double getPitBase() {
		return pitBase;
	}
	public void setPitBase(double pitBase) {
		this.pitBase = pitBase;
	}
	public double getPit() {
		return pit;
	}
	public void setPit(double pit) {
		this.pit = pit;
	}
	public double getCosts() {
		return costs;
	}
	public void setCosts(double costs) {
		this.costs = costs;
	}
	
	
}
